package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    void add(@Nullable AbstractCommand command);

    AbstractCommand getCommandByName(@Nullable String name);

    AbstractCommand getCommandByArgument(@Nullable String argument);

}