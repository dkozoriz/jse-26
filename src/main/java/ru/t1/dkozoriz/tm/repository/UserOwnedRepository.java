package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class UserOwnedRepository<T extends UserOwnedModel> extends AbstractRepository<T>
        implements IUserOwnedRepository<T> {

    public void clear(@Nullable final String userId) {
        @NotNull final List<T> userModels = findAll(userId);
        removeAll(userModels);
    }

    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    public List<T> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<T> result = new ArrayList<>();
        return findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    public T findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return findAll()
                .stream()
                .filter(m -> id.equals(m.getId()))
                .filter(m -> userId.equals(m.getUserId()))
                .findFirst().orElse(null);
    }

    @Nullable
    public T findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return 0;
        int count = 0;
        return (int)(findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count());
    }

    @Nullable
    public T removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final T model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    public T removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    public T add(@Nullable final String userId, @NotNull final T model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    public T remove( @Nullable final String userId, @Nullable final T model) {
        if (userId == null || userId.isEmpty()) return null;
        return removeById(userId, model.getId());
    }
}