package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.api.service.IAbstractService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;

public abstract class AbstractService<T extends AbstractModel, R extends IAbstractRepository<T>> implements IAbstractService<T> {

    @NotNull
    protected final R repository;

    @NotNull
    protected abstract String getName();

    public AbstractService(@NotNull final R abstractRepository) {
        this.repository = abstractRepository;
    }

    @NotNull
    public T add(@Nullable final T model) {
        if (model == null) throw new EntityException(getName());
        return repository.add(model);
    }

    public void clear() {
        repository.clear();
    }

    @NotNull
    public List<T> findAll() {
        return repository.findAll();
    }

    @Nullable
    public T remove(@Nullable final T model) {
        if (model == null) throw new EntityException(getName());
        return repository.remove(model);
    }

    @Nullable
    public T removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final T model = findById(id);
        return remove(model);
    }

    @NotNull
    public T findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = repository.findById(id);
        if (model == null) throw new EntityException(getName());
        return model;
    }

    public int getSize() {
        return repository.getSize();
    }

}