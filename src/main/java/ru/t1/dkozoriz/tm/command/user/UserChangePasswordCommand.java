package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public UserChangePasswordCommand() {
        super("change-user-password", "change password of current user.");
    }

    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        @Nullable final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}