package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public UserLogoutCommand() {
        super("logout", "logout current user.");
    }

    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}